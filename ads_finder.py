#
# Author: Steven Dumolt
#
# Created: 10/1/2015
# Updated: 10/13/2015
#
# This script accepts either a file or a directory as input.  It then identifies
# if the file or files have alternate data streams.  If the alternate data stream
# is the Zone.Identifier it will read it and display the Security Zone in the Sec Zone
# column.  It will hash all streams, collect the create date/time information about the stream
# and get the size stream.  It also attempts to identify the MimType of the stream, this functionality
# could be swapped out for more detailed signature matching methods.
#

import os
import argparse
import hashlib
import time
import mimetypes
import re
from ctypes import *

# Trys to import the tabulate module.
try:
    from tabulate import tabulate
    tab_mod = True
except:
    tab_mod = False
    print 'You should install the tabulate module for better output.'

# Creates and instance of kernel32
kernel = windll.kernel32

# Defines constants that point to size declarations
LPSTR     = c_wchar_p
DWORD     = c_ulong
LONG      = c_ulong
WCHAR     = c_wchar * 255
LONGLONG  = c_longlong

# Builds structures to hold the appropriate Windows API structures.
# These are based on the Window's APIs located at MSDN.microsoft.com
class LargeIntegerUnion(Structure):
    _fields_ = [
        ("LowPart", DWORD),
        ("HighPart", LONG),]

class LargeInteger(Union):
    _fields_ = [
        ("large1", LargeIntegerUnion),
        ("large2", LargeIntegerUnion),
        ("QuadPart",    LONGLONG),
    ]

class Win32FindStreamData(Structure):
    _fields_ = [
        ("StreamSize", LargeInteger),
        ("cStreamName", WCHAR),
    ]

# Hashes the Alternate Data Stream using various methods
def md5HashFile(inFile):
    data = open(inFile,'rb').read()
    md5 = hashlib.md5()
    md5.update(data)
    return md5.hexdigest().upper()

def sha1HashFile(inFile):
    data = open(inFile,'rb').read()
    sha1 = hashlib.sha1()
    sha1.update(data)
    return sha1.hexdigest().upper()


def humanize(num):
    """Converts Bytes to other measurements.

    :param num: Size in bytes
    :return: Returns the highest type of measurement for the number of bytes passed
    """
    for unit in ['Bytes','KB','MB','GB','TB','PB','EB','ZB']:
        if abs(num) < 1024.0:
            return "%3.1f %s" % (num, unit)
        num /= 1024.0
    return bytes

def GetZone(filePath):
    """Identifies the security zone of the file

    :param filePath: File to check the Zone
    :return: A string of either the Zone Information or an empty string.
    """
    data = open(filePath).read()
    zone_type = {-1: 'No Zone', 0: 'My Computer', 1: 'Intranet', 2: 'Trusted', 3: 'Internet', 4: 'Untrusted'}

    try:
        zone_info = re.search(r'\[ZoneTransfer\]\nZoneId=([-1-4])', data)
        return zone_type.get(int(zone_info.group(1)))
    except:
        return ""

def getStreams(filePath, human_output=False):
    """Gets the ADS for a file and returns information about it.

    :param filePath: File to check strings
    :param human_output(switch): Converts bytes to KB, MB, GB, TB, PB, EB, or ZB
    :return: List of stream information.
    """

    # Creates as instance of a Win32 Stream object
    file_infos = Win32FindStreamData()

    # List to hold information on all the streams
    sList=[]

    try:
        # Calls the Windows API to get the first Stream
        myhandler = kernel.FindFirstStreamW (c_wchar_p(filePath), 0, byref(file_infos), 0)

        if file_infos:
            # Calls the Windows API to get the first ADS
            while kernel.FindNextStreamW(myhandler, byref(file_infos)):
                # Sets a variable to the full path to the ADS
                fileStreamPath = '%s:%s' % (filePath,file_infos.cStreamName.split(":")[1])

                # Retrieves owner and time information about the stream
                (mode, ino, dev, nlink, uid, gid,
                 size, atime, mtime, ctime) = os.stat('%s:%s' % (filePath, file_infos.cStreamName.split(":")[1]))

                # Sets variables to hold information about the file.
                stream = str(fileStreamPath)
                createdAt = time.ctime(ctime)
                accessedAt = time.ctime(atime)
                modifiedAt = time.ctime(mtime)

                # If the humanize switch was passed it calls the method and set the variable
                # to the return value.  If the switch was not used, the variable is set to the
                # file size in bytes.
                streamSize = humanize(size) if human_output else size
                mimeType = mimetypes.guess_type(fileStreamPath)[0] if mimetypes.guess_type(fileStreamPath)[0] else 'Unknown'
                md5Hash =  md5HashFile(fileStreamPath)
                sha1Hash = sha1HashFile(fileStreamPath)

                if 'Zone.Identifier' in fileStreamPath:
                    securityZone = GetZone(str(fileStreamPath))
                else:
                    securityZone = ""
                # --- End File information

                # Adds the file stream information as a list to the list of stream.
                sList.append([stream,createdAt, accessedAt, modifiedAt, str(streamSize), securityZone, mimeType, md5Hash, sha1Hash])
        # Returns the list of streams identified for this file
        return sList
    except WindowsError, winError:
        print "Windows error was received processing file %s." % filePath
        return ()



def main():

    # Sets up the argument processing
    parser = argparse.ArgumentParser('Identify alternate data streams and displays information about the file')
    searchCriteria = parser.add_mutually_exclusive_group(required=True)
    searchCriteria.add_argument('--file', help='Search an individual file for alternate data streams')
    searchCriteria.add_argument('--dir', help='Search all files in a directory')
    parser.add_argument('--humanize', action='store_true',
                        help='Converts the number of bytes to human readable measurements. (i.e. KB, MB, ETC...)')

    args = parser.parse_args()

    #  Variable to hold all the file streams for all files processed
    data = []

    # If the file argument was used, it populates data with the streams for the file.
    if args.file:
        data += getStreams(args.file, args.humanize)

    # If the directory argument was used, it will loop through all the files in the directory and
    # populate the data list with the information about each files streams.
    elif args.dir:
        for fileName in os.listdir(args.dir):
            if os.path.isfile(os.path.join(args.dir, fileName)):
                data +=getStreams(os.path.join(args.dir, fileName), args.humanize)


    # Prints a table of the file stream information.

    headers=['ADS', 'CTime', 'ATime', 'MTime', 'Size', 'Sec Zone', 'MimeType', 'md5', 'sha1']

    if data:
        if tab_mod:
            print tabulate(data, headers= headers, tablefmt="simple")
        else:
            print '\t'.join(headers)
            for line in data:
                print '\t'.join(line)



if __name__=='__main__':
    main()